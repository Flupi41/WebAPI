﻿using Microsoft.EntityFrameworkCore;
using WebApplicationPassOrder.Models;

namespace WebApplicationPassOrder
{
    public class MyDBcontext: DbContext
    {
        public DbSet<Users> Users { get; set; }
        public DbSet<Applications> Applications { get; set; }
        public DbSet<Departaments> Departaments { get; set; }
        public DbSet<Employees> Employees { get; set; }
        public DbSet<Divisions> Divisions { get; set; }

        public MyDBcontext(DbContextOptions<MyDBcontext> options): base(options)
        {

        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Users>().ToTable("Users");
            modelBuilder.Entity<Applications>().ToTable("Applications");
            modelBuilder.Entity<Departaments>().ToTable("Departaments");
            modelBuilder.Entity<Employees>().ToTable("Employees");
            modelBuilder.Entity<Divisions>().ToTable("Divisions");
        }
    }
}
