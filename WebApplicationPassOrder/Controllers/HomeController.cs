﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApplicationPassOrder.Models;
using System.Security.Cryptography;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplicationPassOrder.Controllers
{
    public class HomeController : Controller
    {
        //private readonly ILogger<HomeController> _logger;
        MyDBcontext _context;

        public HomeController(MyDBcontext context)
        {
            _context = context; 
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Authorization()
        {
            return View();
        }

        public IActionResult ChooseApplication()
        {
            return View();
        }

        public async Task<IActionResult> PerposalApplication()
        {
            var divisions = await _context.Divisions.ToListAsync();
            ViewBag.DivisionsItems = new SelectList(divisions, "DivisionID", "Tittle");

            var employess = await _context.Employees.ToListAsync();
            ViewBag.EmployeeItems = new SelectList(employess, "EmployeeID", "LastName");

            var applications = await _context.Applications.Include(a => a.Divisions).Include(b => b.Employees).ToListAsync();
            return View(applications.FirstOrDefault());
        }

        public IActionResult GroupApplication()
        {
            return View();
        }

        public IActionResult SubmittedApplications()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register([Bind("Username, Email, Pasword")] Users user)
        {
            if (ModelState.IsValid)
            {
                /*byte[] passwordBytes = Encoding.UTF8.GetBytes(user.Pasword);
                byte[] hashedBytes = MD5.Create().ComputeHash(passwordBytes);
                string hashedPassword = BitConverter.ToString(hashedBytes).Replace("-", "");

                user.Pasword = hashedPassword;*/
                user.UserID = _context.Users.Max(u => u.UserID) + 1;
                _context.Users.Add(user);
                await _context.SaveChangesAsync();
                return Redirect("/Home/ChooseApplication");
            }
            else
            {
                return View("Index");
            }
        }
        [HttpPost]
        public IActionResult Authorization(string username, string pasword)
        {
            var user = _context.Users.FirstOrDefault(u => u.Username == username);
            if (user == null)
            {
                ModelState.AddModelError("Username", "Пользователь не найден.");
                return View();
            }
            if (pasword != user.Pasword)
            {
                ModelState.AddModelError("Pasword", "Неверный пароль.");
                return View();
            }
            /*using (var md5 = MD5.Create())
            {
                byte[] inputBytes = Encoding.UTF8.GetBytes(pasword);
                byte[] hash = md5.ComputeHash(inputBytes);
                string decodedPassword = Convert.ToBase64String(hash);

                if (decodedPassword != user.Pasword)
                {
                    ModelState.AddModelError("Pasword", "Неверный пароль.");
                    return View();
                }
            }*/
            return RedirectToAction("ChooseApplication", "Home");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}