﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplicationPassOrder.Models
{
    public class Applications
    {
        [Key]
        public int ApplicationID { get; set; }
        public string LastName { get; set; }
        public string FistName { get; set; }
        public string ?Patronomyc { get; set; }
        public string ?Phone { get; set; }
        public string Email { get; set; }
        public string ?Organization { get; set; }
        public string Note { get; set; }
        public DateTime DateOfBrith { get; set; }
        public string SeriesPassport { get; set; }
        public string NumberPassport { get; set; }
        [NotMapped]
        public IFormFile ?ImgPassport { get; set; }
        [NotMapped]
        public IFormFile ?ImgUser { get; set; }
        public string TypeApplication { get; set; }
        [ForeignKey("Divisions")]
        public int DivisionID { get; set; }
        public DateTime ?DateSubmission { get; set; }
        public DateTime ?TimeSubmission { get; set; }
        public string Status { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateFinal { get; set; }
        public string PurposeVisit { get; set; }
        [ForeignKey("Employees")]
        public int ?EmployeeID { get; set; }

        public virtual Divisions Divisions { get; set; }
        public virtual Employees Employees { get; set; }


    }
}
