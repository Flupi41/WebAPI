﻿using System.ComponentModel.DataAnnotations;

namespace WebApplicationPassOrder.Models
{
    public class Divisions
    {
        [Key]
        public int DivisionID { get; set; }
        public string Tittle { get; set; }
        public ICollection<Applications> applications { get; set; }
        public ICollection<Employees> employees { get; set; }
    }
}
