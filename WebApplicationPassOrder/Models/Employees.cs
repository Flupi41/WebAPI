﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplicationPassOrder.Models
{
    public class Employees
    {
        [Key]
        public int EmployeeID { get; set; }
        public string LastName { get; set; }
        public string FistName { get; set; }
        public string Patronomyc { get; set; }
        [ForeignKey("Divisions")]
        public int ?DivisionID { get; set; }
        [ForeignKey("Departaments")]
        public int ?DepartamentID { get; set; }
        public string CodeEmployee { get; set; }

        public virtual Divisions Divisions { get; set; }
        public virtual Departaments Departaments { get; set; }
        public ICollection<Applications> applications { get; set; }
    }
}
