﻿using System.ComponentModel.DataAnnotations;

namespace WebApplicationPassOrder.Models
{
    public class Departaments
    {
        [Key]
        public int DepartamentId { get; set; }
        public string Tittle { get; set; }
        public ICollection<Employees> employees { get; set; }
    }
}
