﻿using System.ComponentModel.DataAnnotations;

namespace WebApplicationPassOrder.Models
{
    public class Users
    {
        [Key]
        public int UserID { get; set; }

        [Required(ErrorMessage = ("Вам нужно ввести имя пользователя."))]
        public string Username { get; set; }

        [Required(ErrorMessage = ("Вам нужно ввести электронную почту."))]
        public string Email { get; set; }

        [Required(ErrorMessage = ("Вам нужно ввести пароль."))]
        [StringLength(25, MinimumLength = 8, ErrorMessage = ("Пароль должен содержать не менее 8 символов."))]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\W]).{8,}$", ErrorMessage = "Пароль должен содержать символы разного регистра, цифру и спецсимвол.")]
        public string Pasword { get; set; }

    }
}
